﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CombatController : MonoBehaviour
{
    public CombatController instance;

    public List<HeathSystem> heathSystems;

    private void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void Damage(int d, int hsIndex)
    {

    }

    public int RegisterHS(HeathSystem hs)
    {
        if (!heathSystems.Contains(hs))
            heathSystems.Add(hs);

        return heathSystems.IndexOf(hs);
    }

    public void DeregisterHS(HeathSystem hs)
    {
        int hsIndex = heathSystems.IndexOf(hs);

        if (heathSystems.Contains(hs))
            heathSystems.Remove(hs);

        for(int i = 0; i < heathSystems.Count; i++)
        {
            heathSystems[i].combatControllerID = heathSystems.IndexOf(heathSystems[i]);
        }
    }
}
