﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Atack : MonoBehaviour
{
    public int damage;
    public string tagOfEnemyOfThis;
    public Character c;

    public void Init()
    {
        c = GetComponentInParent<Character>();
    }

    public void HitSuccess(Collider other)
    {
        if (other.CompareTag(tagOfEnemyOfThis))
        {
            c.canRaiseCombo = true;
            c.comboTime = c.maxComboTime;
        }
    }
}
