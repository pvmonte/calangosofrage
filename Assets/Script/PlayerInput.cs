﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInput : MonoBehaviour
{
    public Player player;

    public float speed;
    public float speedX;
    public float speedZ;

    public float moveValue;

    // Start is called before the first frame update
    void Start()
    {
        player = GetComponentInChildren<Player>();
    }

    // Update is called once per frame
    void Update()
    {
        if (player.canReceiveInput)
        {
            float xDir = -Input.GetAxis("Vertical");
            float zDir = Input.GetAxis("Horizontal");
            speedX = xDir * player.speed;
            speedZ = zDir * player.speed;

            if (zDir > 0)
            {
                transform.localScale = new Vector3(1, 1, 1);
            }
            else if (zDir < 0)
            {
                transform.localScale = new Vector3(1, 1, -1);
            }

            if (Input.GetKeyDown(KeyCode.J))
            {
                player.Atack();
            }

            if (player.comboCount == 0)
            {
                transform.Translate(speedX * Time.deltaTime, 0, speedZ * Time.deltaTime);

                if (Input.GetAxisRaw("Vertical") != 0)
                {
                    player.Move(Mathf.Abs(xDir));
                }

                if (Input.GetButtonDown("Jump"))
                {
                    player.Jump();
                    print("JumpInput");
                }

                if (zDir != 0 || xDir != 0)
                {
                    moveValue += Time.deltaTime * 4;
                    moveValue = Mathf.Clamp01(moveValue);
                    player.Move(moveValue);
                }
                else
                {
                    moveValue -= Time.deltaTime * 4;
                    moveValue = Mathf.Clamp01(moveValue);
                    player.Move(moveValue);
                }
            }
        }
    }
}
