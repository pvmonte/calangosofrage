﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour
{
    public float speed;
    public Vector3 jumpForce;
    Rigidbody rb;
    public Animator anim;
    public bool grounded;
    public LayerMask floorLayer;

    public float atackCooldown;
    public float maxAtackCooldown;
    public int comboCount;
    public int maxCombo;
    public float comboTime;
    public float maxComboTime;
    public bool canRaiseCombo;

    public float walking;
    public bool isWalking;
    public bool isAtacking;

    public bool canReceiveInput;
    public string enemyAtackTag;

    // Start is called before the first frame update
    void Start()
    {
        Init();
    }

    public void Init()
    {
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        OnUpdate();
    }

    public void OnUpdate()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Damage") || anim.GetCurrentAnimatorStateInfo(0).IsName("Fallen") || anim.GetCurrentAnimatorStateInfo(0).IsName("GettingUp"))
            canReceiveInput = false;
        else
            canReceiveInput = true;

        grounded = Physics.Raycast(transform.position + Vector3.up * 0.05f, Vector3.down, 0.1f, floorLayer);

        if (comboTime >= 0)
        {
            comboTime -= Time.deltaTime;
        }
        else
        {
            canRaiseCombo = false;
            //isAtacking = false;
            comboTime = 0;
            comboCount = 0;
            maxComboTime = 0.25f;
            anim.SetInteger("combo", comboCount);
        }

        atackCooldown -= Time.deltaTime;

        if (atackCooldown <= 0)
        {
            atackCooldown = 0;
        }

        anim.SetBool("jump", !grounded);
        anim.SetFloat("walking", walking);
    }

    public void Jump()
    {
        StartCoroutine(JumpCorout());
    }

    IEnumerator JumpCorout()
    {
        yield return new WaitForSeconds(0.185f);
        rb.AddForce(jumpForce);
        print("Jump");
    }

    public void Move(float value)
    {
        print("walkingUp");
        walking = value;
        isWalking = true;
    }

    public void Atack()
    {
            StartCoroutine(AtackCorout());
    }

    IEnumerator AtackCorout()
    {
        if (atackCooldown <= 0)
        {
            if (canRaiseCombo || comboCount == 0)
                comboCount += 1;

            isAtacking = true;
            comboTime = maxComboTime;
            anim.SetInteger("combo", comboCount);
            anim.SetTrigger("atack");
            anim.SetBool("walking", false);
            atackCooldown = maxAtackCooldown;

            yield return new WaitForSeconds(1.25f);

            isAtacking = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag(enemyAtackTag))
        {
            anim.Play("Damage");
        }
    }
}
