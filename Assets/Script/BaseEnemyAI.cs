﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseEnemyAI : MonoBehaviour
{
    public enum AiState
    {
        Waiting,
        Chasing,
        Atacking
    }

    public AiState aiState;
    public List<Player> players;

    public Character character;

    public Transform target;
    public Vector3 distVector;

    // Start is called before the first frame update
    void Start()
    {
        character = GetComponentInChildren<Character>();
    }

    // Update is called once per frame
    void Update()
    {
        if (!character.isAtacking)
        {
            if (target)
            {
                distVector = target.position - transform.position;

                if (Mathf.Abs(distVector.x) > 2 || Mathf.Abs(distVector.z) > 0.2f)
                {
                    aiState = AiState.Chasing;
                }
                else
                {
                    aiState = AiState.Atacking;
                }
            }
            else
            {
                aiState = AiState.Waiting;
            }

            if (!character.isAtacking)
                SwitchAiState();
        }
    }

    public void SwitchAiState()
    {
        float moveSpeed = 0;

        switch (aiState)
        {
            case AiState.Waiting:
                print("Waiting");
                character.anim.SetBool("walking", false);
                break;
            case AiState.Chasing:
                print("Chasing");

                moveSpeed = 0;
                character.anim.SetBool("walking", true);

                if (target.position.z > transform.position.z + 0.1f)
                {
                    Vector3 newPos = transform.position;
                    newPos.z += character.speed * Time.deltaTime;
                    transform.position = newPos;
                }
                else if (target.position.z < transform.position.z - 0.1f)
                {
                    Vector3 newPos = transform.position;
                    newPos.z -= character.speed * Time.deltaTime;
                    transform.position = newPos;
                }

                if (transform.position.x < target.position.x - 2)
                {
                    print("Menor");
                    moveSpeed = character.speed;
                    transform.Translate(0, 0, moveSpeed * Time.deltaTime);
                    transform.localScale = Vector3.one;
                }
                else if (transform.position.x > target.position.x - 2 && transform.position.x < target.position.x)
                {
                    Vector2 newPos = transform.position;
                    newPos.x = target.position.x - 2;
                    transform.localScale = Vector3.one;
                }
                else if (transform.position.x > target.position.x + 2)
                {
                    print("Maior");
                    moveSpeed = character.speed * -1;
                    transform.Translate(0, 0, moveSpeed * Time.deltaTime);
                    transform.localScale = new Vector3(1, 1, -1);
                }
                else if (transform.position.x < target.position.x + 2 && transform.position.x > target.position.x)
                {
                    Vector2 newPos = transform.position;
                    newPos.x = target.position.x + 2;
                }

                break;
            case AiState.Atacking:
                print("Atacking");
                moveSpeed = 0;
                character.Atack();
                break;
        }
    }

    public void SelectTarget()
    {
        if (players.Count == 1)
        {
            target = players[0].transform;
        }
        else if (players.Count >= 2)
        {
            float[] distances = new float[players.Count];
            float minDist = 0;
            int minDistIndex = 0;

            for (int i = 0; i < players.Count; i++)
            {
                distances[i] = (transform.position - players[i].transform.position).sqrMagnitude;

                if (i == 0)
                {
                    minDist = distances[i];
                    minDistIndex = i;
                }
                else
                {
                    if (distances[i] < minDist)
                    {
                        minDist = distances[i];
                        minDistIndex = i;
                    }
                }
            }
        }
        else
        {
            target = null;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            print("Hit");
            players.Add(other.gameObject.GetComponent<Player>());
            SelectTarget();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            print("Saiu");
            players.Remove(other.gameObject.GetComponent<Player>());
            SelectTarget();
        }
    }
}
