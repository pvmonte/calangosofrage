﻿//Toon - Rim - Shading Outline Softness-

Shader "Custom/BumpSpecular" {
	Properties{
		_Color("Color", Color) = (0,0,0,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}		//_Glossiness("Smoothness", Range(0,1)) = 0.5
		_NormalMap("NormalMap", 2D) = "White" {} //ADD		//_Metallic("Metallic", Range(0,1)) = 0.0

		_SpecMap("Specular Map", 2D) = "White" {}
		_Glossiness("Gloss", range(1, 8)) = 2

		_Shadding("Shadding", range(0,1)) = 0.5
		_Lighting("Lighting", range(0,1)) = 0.5

		_Outline("Outline", range(0,1)) = 0.1
		_OutlineColor("Outline Color", Color) = (0,0,0,1)

		_ShadingSoftness("Shading Softness", range(0,1)) = 0
		_OutlineSoftness("Outline Softness", range(0,1)) = 0
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
	#pragma surface surf SpecMap
	#pragma target 3.0

	half4 _Color;
	sampler2D _MainTex;
	sampler2D _NormalMap;

	sampler2D _SpecMap;
	half _Glossiness;

	half _Shadding;
	half _Lighting;

	half _Outline;
	half _OutlineColor;

	half _ShadingSoftness;
	half _OutlineSoftness;

	struct MySurfaceOutput
	{
		half3 Albedo;
		half3 Normal;
		half3 Emission;
		half Specular;
		half Alpha;

		half Shadding;
		half Lighting;

		half3 GlossiColor;
	};


	half halfDiffuseTerm(half3 a, half3 b)
	{
		return dot(normalize(a), normalize(b)) * 0.5f * 0.5f;
	}

	//Add Lighting
	half diffuseTerm(half3 a, half3 b)
	{
		return max(0, dot(normalize(a), normalize(b)));
	}

	half softStep(half a, half b, half softness)
	{
		return step(a,b) * clamp((b-a)/softness,0,1);//return step(a,b) * clamp((b+a)/softness,0,1);
	}


	half4 LightingSpecMap(MySurfaceOutput o, half3 lightDir, half3 viewDir, half atten)
	{
		half d = halfDiffuseTerm(o.Normal, lightDir);

		half shade = softStep(_Shadding, d, _ShadingSoftness);

		half3 diffuseColor = _LightColor0 * o.Albedo * shade;

		half s = pow(diffuseTerm(o.Normal, lightDir + viewDir), _Glossiness);

		half light = softStep(_Lighting, s, _ShadingSoftness);

		half3 specularColor = _LightColor0 * o.GlossiColor * light;

		half3 returnColor = (diffuseColor + specularColor) * atten * 2;

		return half4(returnColor, o.Alpha);
	}

	struct Input
	{
		float2 uv_MainTex;
		float3 viewDir;
	};

	void surf(Input IN, inout MySurfaceOutput o)
	{
		half4 c = tex2D(_MainTex, IN.uv_MainTex);

		half3 n = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));
		o.Normal = n;

		half d = dot(normalize(IN.viewDir), o.Normal);

		half Line = softStep(_Outline, d, _ShadingSoftness);

		o.Albedo = (c.rgb * _Color * Line) + ((1 - Line) * _OutlineColor);

		o.Alpha = c.a;

		half4 s = tex2D(_SpecMap, IN.uv_MainTex);
		o.GlossiColor = s.rgb;
	}
	ENDCG
	}
		FallBack "Diffuse"
}


//Toon - Rim - Outline //Diferenc
/*
Shader "Custom/BumpSpecular" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}		//_Glossiness("Smoothness", Range(0,1)) = 0.5
		_NormalMap("NormalMap", 2D) = "White" {} //ADD		//_Metallic("Metallic", Range(0,1)) = 0.0
		
		_SpecMap("Specular Map", 2D) = "White" {}
		_Glossiness("Gloss", range (1, 8)) = 2

		_Shadding("Shadding", range (0,1)) = 0.5
		_Lighting("Lighting", range (0,1)) = 0.5

		_Outline("Outline", range(0,1)) = 0.1
		_OutlineColor("Outline Color", Color) = (0,0,0,1)
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			#pragma surface surf SpecMap

			half4 _Color;
			sampler2D _MainTex;
			sampler2D _NormalMap;

			sampler2D _SpecMap;
			half _Glossiness;

			half _Shadding;
			half _Lighting;

			half _Outline;
			half _OutlineColor;

			struct MySurfaceOutput
			{
				half3 Albedo;
				half3 Normal;
				half3 Emission;
				half Specular;
				half Alpha;

				half Shadding;
				half Lighting;

				half3 GlossiColor;
			};


		half halfDiffuseTerm(half3 a, half3 b)
		{
			return dot (normalize(a), normalize(b)) * 0.5f * 0.5f;
		}

		//Add Lighting
		half diffuseTerm (half3 a, half3 b)
		{
			return max(0, dot(normalize(a), normalize(b)));
		}

		half4 LightingSpecMap(MySurfaceOutput o, half3 lightDir, half3 viewDir, half atten)
		{
			half d = halfDiffuseTerm(o.Normal, lightDir);

			half shade = step(_Shadding, d);

			half3 diffuseColor = _LightColor0 * o.Albedo * shade;

			half s = pow(diffuseTerm(o.Normal, lightDir + viewDir), _Glossiness);

			half light = step(_Lighting, s);

			half3 specularColor = _LightColor0 * o.GlossiColor * light;

			half3 returnColor = (diffuseColor + specularColor) * atten * 2;

			return half4(returnColor, o.Alpha);
		}

		struct Input
		{
			float2 uv_MainTex;
			float3 viewDir;
		};

		void surf (Input IN, inout MySurfaceOutput o)
		{
			half4 c = tex2D (_MainTex, IN.uv_MainTex);

			half3 n = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));
			o.Normal = n;

			half d = dot(normalize(IN.viewDir), o.Normal);

			half Line = step (_Outline, d);
			//o.Albedo = (c.rgb * _Color * line) = ((l line) * _OutlineColor);	
			//o.Albedo = (c.rgb * _Color * line) + ((1 - line) * _OutlineColor);
			
			o.Albedo = (c.rgb * _Color * Line) + ((1 - Line) * _OutlineColor);
			
			o.Alpha = c.a;

			half4 s = tex2D(_SpecMap, IN.uv_MainTex);
			o.GlossiColor = s.rgb;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
*/

//PT1
/*
Shader "Custom/BumpSpecular" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}		//_Glossiness("Smoothness", Range(0,1)) = 0.5
	_NormalMap("NormalMap", 2D) = "White" {} //ADD		//_Metallic("Metallic", Range(0,1)) = 0.0

	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
#pragma surface surf Lambert

		half4 _Color;
	sampler2D _MainTex;
	sampler2D _NormalMap;

	struct Input {
		float2 uv_MainTex;
	};

	void surf(Input IN, inout SurfaceOutput o)
	{
		half4 c = tex2D(_MainTex, IN.uv_MainTex);
		o.Albedo = c.rgb;
		o.Alpha = c.a;

		half3 n = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));

		o.Normal = n;
	}
	ENDCG
	}
		FallBack "Diffuse"
}
*/

//Bump Specular PT2
/*
Shader "Custom/BumpSpecular" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}		//_Glossiness("Smoothness", Range(0,1)) = 0.5
		_NormalMap("NormalMap", 2D) = "White" {} //ADD		//_Metallic("Metallic", Range(0,1)) = 0.0
		_SpecMap("Specular Map", 2D) = "White" {}
		_Glossiness("Gloss", range (1,4)) = 2
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			#pragma surface surf SpecMap

			half4 _Color;
			sampler2D _MainTex;
			sampler2D _NormalMap;

			sampler2D _SpecMap;
			half _Glossiness;

			struct MySurfaceOutput
			{
				half3 Albedo;
				half3 Normal;
				half3 Emission;
				half Specular;
				half Alpha;

				half3 GlossiColor;
			};

		//Add Lighting
		half diffuseTerm (half3 a, half3 b)
		{
			return max(0, dot(normalize(a), normalize(b)));
		}

		half4 LightingSpecMap(MySurfaceOutput o, half3 lightDir, half3 viewDir, half atten)
		{
			half d = diffuseTerm(o.Normal, lightDir);
			half3 diffuseColor = _LightColor0 * o.Albedo * d;

			half s = diffuseTerm(o.Normal, lightDir + viewDir);
			half3 specularColor = _LightColor0 * o.GlossiColor * s;

			half3 returnColor = (diffuseColor + specularColor) * atten * 2;

			return half4(returnColor, o.Alpha);
		}

		struct Input
		{
			float2 uv_MainTex;
		};

		void surf (Input IN, inout MySurfaceOutput o)
		{
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb * _Color;//Add _Color 
			o.Alpha = c.a;

			half3 n =  UnpackNormal( tex2D(_NormalMap, IN.uv_MainTex));
			o.Normal = n;

			half4 s = tex2D(_SpecMap, IN.uv_MainTex);
			o.GlossiColor = s.rgb;
		}
		ENDCG
	}
	FallBack "Diffuse"
	}
*/

//Bump Specular
/*
Shader "Custom/BumpSpecular" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}		//_Glossiness("Smoothness", Range(0,1)) = 0.5
		_NormalMap("NormalMap", 2D) = "White" {} //ADD		//_Metallic("Metallic", Range(0,1)) = 0.0
		_SpecMap("Specular Map", 2D) = "White" {}
		_Glossiness("Gloss", range (1, 8)) = 2
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			#pragma surface surf SpecMap

			half4 _Color;
			sampler2D _MainTex;
			sampler2D _NormalMap;

			sampler2D _SpecMap;
			half _Glossiness;

			struct MySurfaceOutput
			{
				half3 Albedo;
				half3 Normal;
				half3 Emission;
				half Specular;
				half Alpha;

				half3 GlossiColor;
			};

		//Add Lighting
		half diffuseTerm (half3 a, half3 b)
		{
			return max(0, dot(normalize(a), normalize(b)));
		}

		half4 LightingSpecMap(MySurfaceOutput o, half3 lightDir, half3 viewDir, half atten)
		{
			half d = diffuseTerm(o.Normal, lightDir);
			half3 diffuseColor = _LightColor0 * o.Albedo * d;

			half s = pow(diffuseTerm(o.Normal, lightDir + viewDir), _Glossiness);
			half3 specularColor = _LightColor0 * o.GlossiColor * s;

			half3 returnColor = (diffuseColor + specularColor) * atten * 2;

			return half4(returnColor, o.Alpha);
		}

		struct Input
		{
			float2 uv_MainTex;
		};

		void surf (Input IN, inout MySurfaceOutput o)
		{
			half4 c = tex2D (_MainTex, IN.uv_MainTex);
			o.Albedo = c.rgb * _Color;//Add _Color 
			o.Alpha = c.a;

			half3 n =  UnpackNormal( tex2D(_NormalMap, IN.uv_MainTex));
			o.Normal = n;

			half4 s = tex2D(_SpecMap, IN.uv_MainTex);
			o.GlossiColor = s.rgb;
		}
		ENDCG
	}
	FallBack "Diffuse"
	}
*/

//ToonShade and Lighting//
/*
Shader "Custom/BumpSpecular" {
	Properties{
		_Color("Color", Color) = (1,1,1,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}		//_Glossiness("Smoothness", Range(0,1)) = 0.5
	_NormalMap("NormalMap", 2D) = "White" {} //ADD		//_Metallic("Metallic", Range(0,1)) = 0.0

	_SpecMap("Specular Map", 2D) = "White" {}
	_Glossiness("Gloss", range(1, 8)) = 2

		_Shadding("Shadding", range(0,1)) = 0.5
		_Lighting("Lighting", range(0,1)) = 0.5
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
#pragma surface surf SpecMap

		half4 _Color;
	sampler2D _MainTex;
	sampler2D _NormalMap;

	sampler2D _SpecMap;
	half _Glossiness;

	half _Shadding;
	half _Lighting;

	struct MySurfaceOutput
	{
		half3 Albedo;
		half3 Normal;
		half3 Emission;
		half Specular;
		half Alpha;

		half Shadding;
		half Lighting;

		half3 GlossiColor;
	};


	half halfDiffuseTerm(half3 a, half3 b)
	{
		return dot(normalize(a), normalize(b)) * 0.5f * 0.5f;
	}

	//Add Lighting
	half diffuseTerm(half3 a, half3 b)
	{
		return max(0, dot(normalize(a), normalize(b)));
	}

	half4 LightingSpecMap(MySurfaceOutput o, half3 lightDir, half3 viewDir, half atten)
	{
		half d = halfDiffuseTerm(o.Normal, lightDir);

		half shade = step(_Shadding, d);

		half3 diffuseColor = _LightColor0 * o.Albedo * shade;

		half s = pow(diffuseTerm(o.Normal, lightDir + viewDir), _Glossiness);

		half light = step(_Lighting, s);

		half3 specularColor = _LightColor0 * o.GlossiColor * light;

		half3 returnColor = (diffuseColor + specularColor) * atten * 2;

		return half4(returnColor, o.Alpha);
	}

	struct Input
	{
		float2 uv_MainTex;
	};

	void surf(Input IN, inout MySurfaceOutput o)
	{
		half4 c = tex2D(_MainTex, IN.uv_MainTex);
		o.Albedo = c.rgb * _Color;//Add _Color 
		o.Alpha = c.a;

		half3 n = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));
		o.Normal = n;

		half4 s = tex2D(_SpecMap, IN.uv_MainTex);
		o.GlossiColor = s.rgb;
	}
	ENDCG
	}
		FallBack "Diffuse"
}
*/

//Toon - Rim - Outline

/*
Shader "Custom/BumpSpecular" {
	Properties {
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}		//_Glossiness("Smoothness", Range(0,1)) = 0.5
		_NormalMap("NormalMap", 2D) = "White" {} //ADD		//_Metallic("Metallic", Range(0,1)) = 0.0
		
		_SpecMap("Specular Map", 2D) = "White" {}
		_Glossiness("Gloss", range (1, 8)) = 2

		_Shadding("Shadding", range (0,1)) = 0.5
		_Lighting("Lighting", range (0,1)) = 0.5

		_Outline("Outline", range(0,1)) = 0.1
		_OutlineColor("Outline Color", Color) = (0,0,0,1)
	}
		SubShader{
			Tags { "RenderType" = "Opaque" }
			LOD 200

			CGPROGRAM
			#pragma surface surf SpecMap

			half4 _Color;
			sampler2D _MainTex;
			sampler2D _NormalMap;

			sampler2D _SpecMap;
			half _Glossiness;

			half _Shadding;
			half _Lighting;

			half _Outline;
			half _OutlineColor;

			struct MySurfaceOutput
			{
				half3 Albedo;
				half3 Normal;
				half3 Emission;
				half Specular;
				half Alpha;

				half Shadding;
				half Lighting;

				half3 GlossiColor;
			};


		half halfDiffuseTerm(half3 a, half3 b)
		{
			return dot (normalize(a), normalize(b)) * 0.5f * 0.5f;
		}

		//Add Lighting
		half diffuseTerm (half3 a, half3 b)
		{
			return max(0, dot(normalize(a), normalize(b)));
		}

		half4 LightingSpecMap(MySurfaceOutput o, half3 lightDir, half3 viewDir, half atten)
		{
			half d = halfDiffuseTerm(o.Normal, lightDir);

			half shade = step(_Shadding, d);

			half3 diffuseColor = _LightColor0 * o.Albedo * shade;

			half s = pow(diffuseTerm(o.Normal, lightDir + viewDir), _Glossiness);

			half light = step(_Lighting, s);

			half3 specularColor = _LightColor0 * o.GlossiColor * light;

			half3 returnColor = (diffuseColor + specularColor) * atten * 2;

			return half4(returnColor, o.Alpha);
		}

		struct Input
		{
			float2 uv_MainTex;
			float3 viewDir;
		};

		void surf (Input IN, inout MySurfaceOutput o)
		{
			half4 c = tex2D (_MainTex, IN.uv_MainTex);

			half3 n = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));
			o.Normal = n;

			half d = dot(normalize(IN.viewDir), o.Normal);

			half Line = step (_Outline, d);
			//o.Albedo = (c.rgb * _Color * line) = ((l line) * _OutlineColor);	
			//o.Albedo = (c.rgb * _Color * line) + ((1 - line) * _OutlineColor);
			
			o.Albedo = (c.rgb * _Color * Line) + ((1 - Line) * _OutlineColor);
			
			o.Alpha = c.a;

			half4 s = tex2D(_SpecMap, IN.uv_MainTex);
			o.GlossiColor = s.rgb;
		}
		ENDCG
	}
	FallBack "Diffuse"
}
*/

//Toon - Rim - Shading Outline Softness-
/*
Shader "Custom/BumpSpecular" {
	Properties{
		_Color("Color", Color) = (0,0,0,1)
		_MainTex("Albedo (RGB)", 2D) = "white" {}		//_Glossiness("Smoothness", Range(0,1)) = 0.5
	_NormalMap("NormalMap", 2D) = "White" {} //ADD		//_Metallic("Metallic", Range(0,1)) = 0.0

	_SpecMap("Specular Map", 2D) = "White" {}
	_Glossiness("Gloss", range(1, 8)) = 2

		_Shadding("Shadding", range(0,1)) = 0.5
		_Lighting("Lighting", range(0,1)) = 0.5

		_Outline("Outline", range(0,1)) = 0.1
		_OutlineColor("Outline Color", Color) = (0,0,0,1)

		_ShadingSoftness("Shading Softness", range(0,1)) = 0
		_OutlineSoftness("Outline Softness", range(0,1)) = 0
	}
		SubShader{
		Tags{ "RenderType" = "Opaque" }
		LOD 200

		CGPROGRAM
#pragma surface surf SpecMap
#pragma target 3.0

		half4 _Color;
	sampler2D _MainTex;
	sampler2D _NormalMap;

	sampler2D _SpecMap;
	half _Glossiness;

	half _Shadding;
	half _Lighting;

	half _Outline;
	half _OutlineColor;

	half _ShadingSoftness;
	half _OutlineSoftness;

	struct MySurfaceOutput
	{
		half3 Albedo;
		half3 Normal;
		half3 Emission;
		half Specular;
		half Alpha;

		half Shadding;
		half Lighting;

		half3 GlossiColor;
	};


	half halfDiffuseTerm(half3 a, half3 b)
	{
		return dot(normalize(a), normalize(b)) * 0.5f * 0.5f;
	}

	//Add Lighting
	half diffuseTerm(half3 a, half3 b)
	{
		return max(0, dot(normalize(a), normalize(b)));
	}

	half softStep(half a, half b, half softness)
	{
		return step(a,b) * clamp((b - a) / softness,0,1);//return step(a,b) * clamp((b+a)/softness,0,1);
	}


	half4 LightingSpecMap(MySurfaceOutput o, half3 lightDir, half3 viewDir, half atten)
	{
		half d = halfDiffuseTerm(o.Normal, lightDir);

		half shade = softStep(_Shadding, d, _ShadingSoftness);

		half3 diffuseColor = _LightColor0 * o.Albedo * shade;

		half s = pow(diffuseTerm(o.Normal, lightDir + viewDir), _Glossiness);

		half light = softStep(_Lighting, s, _ShadingSoftness);

		half3 specularColor = _LightColor0 * o.GlossiColor * light;

		half3 returnColor = (diffuseColor + specularColor) * atten * 2;

		return half4(returnColor, o.Alpha);
	}

	struct Input
	{
		float2 uv_MainTex;
		float3 viewDir;
	};

	void surf(Input IN, inout MySurfaceOutput o)
	{
		half4 c = tex2D(_MainTex, IN.uv_MainTex);

		half3 n = UnpackNormal(tex2D(_NormalMap, IN.uv_MainTex));
		o.Normal = n;

		half d = dot(normalize(IN.viewDir), o.Normal);

		half Line = softStep(_Outline, d, _ShadingSoftness);

		o.Albedo = (c.rgb * _Color * Line) + ((1 - Line) * _OutlineColor);

		o.Alpha = c.a;

		half4 s = tex2D(_SpecMap, IN.uv_MainTex);
		o.GlossiColor = s.rgb;
	}
	ENDCG
	}
		FallBack "Diffuse"
}
*/